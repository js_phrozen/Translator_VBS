Dim source, target, where

Set oFSO = CreateObject("Scripting.FileSystemObject")

source = InputBox("Folder with original files [X:\source\]:")
target = InputBox("Folder with sorted files [X:\target\]:")
where = InputBox("Folder with files to sort [X:\all_files\]:")

Function walkFolder(strFolder)

    Set oFolder = oFSO.GetFolder(strFolder)
    Set oSubFolders = oFolder.SubFolders

    For Each oDir in oSubFolders
        strTargetFolder = Replace(oDir.Path, source, target,1,1,1)
        If Not oFSO.FolderExists(strTargetFolder) Then 
          oFSO.CreateFolder(strTargetFolder)
        End If
        walkFolder(oDir.Path)
    Next
      For Each oFile In oFolder.Files
          strSourceFile = where & oFile.Name
          strTargetFile = Replace(oFile.Path, source, target,1,1,1)
          If oFSO.FileExists(strSourceFile) Then
                oFSO.MoveFile strSourceFile, strTargetFile
          End If
          
          If oFSO.FileExists(strSourceFile & ".ttx") Then
                oFSO.MoveFile strSourceFile & ".ttx", strTargetFile & ".ttx"
          End If
    Next
    
End Function


If (Right(source, 1) = "\") And (Right(target, 1) = "\") And (Right(where, 1) = "\") Then
    walkFolder(source)
    MsgBox "Done!"
Else
  MsgBox "Folder name must end with backslash '\'"     
End If


