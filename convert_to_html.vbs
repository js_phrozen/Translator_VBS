Dim iPath, outFile, objDoc

    Const wdFormatDocument                    =  0
    Const wdFormatDocument97                  =  0
    Const wdFormatDocumentDefault             = 16
    Const wdFormatDOSText                     =  4
    Const wdFormatDOSTextLineBreaks           =  5
    Const wdFormatEncodedText                 =  7
    Const wdFormatFilteredHTML                = 10
    Const wdFormatFlatXML                     = 19
    Const wdFormatFlatXMLMacroEnabled         = 20
    Const wdFormatFlatXMLTemplate             = 21
    Const wdFormatFlatXMLTemplateMacroEnabled = 22
    Const wdFormatHTML                        =  8
    Const wdFormatPDF                         = 17
    Const wdFormatRTF                         =  6
    Const wdFormatTemplate                    =  1
    Const wdFormatTemplate97                  =  1
    Const wdFormatText                        =  2
    Const wdFormatTextLineBreaks              =  3
    Const wdFormatUnicodeText                 =  7
    Const wdFormatWebArchive                  =  9
    Const wdFormatXML                         = 11
    Const wdFormatXMLDocument                 = 12
    Const wdFormatXMLDocumentMacroEnabled     = 13
    Const wdFormatXMLTemplate                 = 14
    Const wdFormatXMLTemplateMacroEnabled     = 15
    Const wdFormatXPS                         = 18

Set oFSO = CreateObject("Scripting.FileSystemObject")
Set wordApp = CreateObject("Word.Application")

iPath = InputBox("File name " & _
    "(eg. c:\path\file.doc), " & _
    "or folder (napr: c:\path\to\files\):")

Function walkFolder(strFolder)

    Set oFolder = oFSO.GetFolder(strFolder)
    Set oSubFolders = oFolder.SubFolders

    For Each oDir in oSubFolders
        walkFolder(oDir.Path)
    Next
    
    For Each oFile In oFolder.Files
        If InStr(oFile.Path, "~$") = 0 Then
        If Right(oFile.Path, 3) = "doc" Or Right(oFile.Path, 3) = "rtf" Or Right(oFile.Path, 3) = "txt" Then
            outFile = oFile.Path & ".html"
            Set objDoc = wordApp.Documents.Open(oFile.Path)
            objDoc.SaveAs outFile, wdFormatHTML
            objDoc.Close
        End If
        End If
    Next
End Function


If Right(iPath, 1) = "\" Then
    walkFolder(iPath)
Else
    If Right(iPath, 3) = "doc" Or Right(iPath, 3) = "rtf" Or Right(iPath, 3) = "txt" Then
           outFile = iPath & ".html"
           Set objDoc = wordApp.Documents.Open(iPath)
           objDoc.SaveAs outFile, wdFormatHTML
           objDoc.Close
    End If
End If
wordApp.Quit
      
MsgBox "Done !"
